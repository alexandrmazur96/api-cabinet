<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->group(['prefix' => '/v1/user'], function ($router) {
    $router->get('/me', 'Api\CabinetController@currentUser');
    $router->get('/{userId}', 'Api\CabinetController@sideUser')->where('userId', '\d+');
    $router->post('/me', 'Api\CabinetController@updateCurrentUser');
    $router->post('/addEmail', 'Api\CabinetController@addUserEmail');
});

$router->group(['prefix' => '/v1/book'], function ($router){
    $router->get('/', 'Api\BookController@getBooks');
    $router->get('/{bookId}', 'Api\BookController@getBook')->where('bookId', '\d+');
    $router->get('/search/{term}', 'Api\BookController@searchBooks');
//        ->where('term', 'a-zA-Zа-яА-Я\d{3,}');
    $router->post('/add-to-user', 'Api\BookController@addBookToUser');
    $router->post('/add-to-store', 'Api\BookController@addBookToStore');
});