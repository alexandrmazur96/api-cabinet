<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:15
 */

namespace App\Traits;

trait TPerson

{
    public function getId() : int{
        return $this->id;
    }

    public function getBooks() : array{
        return $this->userBooks()->get()->getDictionary();
    }

    public function getNameFirst() : string{
        return $this->first_name;
    }

    public function getNameLast() : string{
        return $this->last_name;
    }
}