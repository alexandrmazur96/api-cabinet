<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 24.07.17
 * Time: 16:12
 */

namespace App\Interfaces;


interface IUser extends IPerson
{
    public function getEmails() : array;
    public function session() : IUserSession;
}