<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:02
 */

namespace App\Interfaces;


interface IBookAuthor extends IPerson
{
    public function isMember() : bool;
    public function user() : IUser;
}