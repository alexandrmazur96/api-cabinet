<?php
/**
 * Created by PhpStorm.
 * User: alexandrmazur
 * Date: 27.07.17
 * Time: 15:00
 */

namespace App\Interfaces;


interface IBookHolder
{
    public function getBooks() : array;
}