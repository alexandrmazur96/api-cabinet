<?php

namespace App\Helpers;

use GuzzleHttp\Client;
class Session
{
    /**
     * Return user data where stored in session in signin api
     *
     * @param string $sessionId
     * @return array with user data from session
     */
    public function getSessionData($sessionId): array
    {
        $client = new Client(
            ['base_uri' => 'http://api.signin.ks/']
        );
        return json_decode(
            $client->request(
                'POST',
                '/api/v1/session/getSessionInfo',
                [
                    'headers' => [
                        'x-session-id' => $sessionId
                    ],
                    'http_errors' => false
                ]
            )->getBody()->getContents(),
            true
        );
    }
}