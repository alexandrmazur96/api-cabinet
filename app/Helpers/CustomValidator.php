<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Validator;

class CustomValidator
{

    /**
     * Check session from signin api
     *
     * @param string $sessionId
     * @return int checking status
     */
    public function checkSession($sessionId)
    {
        if ($sessionId === null OR strlen($sessionId) !== 32) {
            return 401;
        }

        $client = new Client([
            'base_uri' => 'http://api.signin.ks'
        ]);
        $statusCode = $client->request('POST',
            '/api/v1/session/check-session',
            [
                'headers' => [
                    'x-session-id' => $sessionId
                ],
                'http_errors' => false
            ])->getStatusCode();
        if ($statusCode === 403) {
            return 403;
        }

        return 200;
    }

    /**
     * Check input email
     *
     * @param string $email input email
     * @return int checking status
     */
    public function checkEmail($email){
        $emailValidValidator = Validator::make(
            $email,
            [
                'email' => 'min:5|email'
            ]
        );

        $emailExistsValidator = Validator::make(
            $email,
            [
                'email' => 'unique:emails,email'
            ]
        );

        if($emailValidValidator->fails()){
            return 400;
        }

        if($emailExistsValidator->fails()){
            return 302;
        }
    }
}