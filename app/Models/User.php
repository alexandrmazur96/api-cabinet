<?php

namespace App\Models;

use App\Interfaces\IUserSession;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Interfaces\IUser;
use App\Traits\TPerson;

class User extends Model implements IUser
{
    use Notifiable;
    use TPerson;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'is_admin',
        'is_author'
    ];

    protected $guarded = [
        'id'
    ];

    private function emails()
    {
        return $this->hasMany('App\Models\Email', 'user_id', 'id');
    }

    private function userBooks()
    {
        return $this->belongsToMany('App\Models\Book', 'users_books');
    }

    public static function authorBooks()
    {
        return User::belongsToMany('App\Models\Book', 'authors_books');
    }

    public function getEmails(): array
    {
        $emails = [];

        //wtf??
        foreach ($this->emails()->get() as $email) {
            array_push($emails, $email->email);
        }

        return $emails;
    }

    public function isAuthor(): bool
    {
        return $this->is_author;
    }

    public function session(): IUserSession
    {
        // TODO: Implement session() method.
    }
}
