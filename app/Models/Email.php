<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'emails';

    protected $fillable = [
        'email',
        'is_main',
        'user_id'
    ];

    protected $guarded = [
        'id'
    ];

    public function password(){
        return $this->hasOne('App\Models\Password');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
