<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBook extends Model
{
    protected $table = 'users_books';

    protected $fillable = [
        'user_id',
        'book_id'
    ];

    protected $guarded = [
        'id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function book(){
        return $this->belongsTo('App\Models\Book', 'book_id');
    }
}
