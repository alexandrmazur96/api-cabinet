<?php

namespace App\Models;

use App\Interfaces\IBookAuthor;
use App\Interfaces\IUser;
use Illuminate\Database\Eloquent\Model;
use App\Traits\TPerson;

class BookAuthor extends Model implements IBookAuthor
{
    use TPerson;

    protected $table = 'authors_books';

    protected $fillable = [
        'user_id',
        'book_id'
    ];

    protected $guarded = [
        'id'
    ];

    private function dbUser()
    {
        return $this->belongsTo('App\Models\BookAuthor');
    }

    public function getBooks() : array
    {
        return User::authorBooks()->get()->getDictionary();
    }

    public function isMember(): bool
    {
        return $this->dbUser->emails()->first()->password !== null;
    }

    public function user(): IUser
    {
        return $this->dbUser;
    }
}
