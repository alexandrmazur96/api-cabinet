<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    protected $table = 'passwords';

    protected $guardable = [
        'id'
    ];

    protected $fillable = [
        'password',
        'email_id'
    ];

    public function email(){
        return $this->belongsTo('App\Models\Email');
    }
}
