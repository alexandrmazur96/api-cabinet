<?php

namespace App\Models;

use App\Interfaces\IBook;
use Illuminate\Database\Eloquent\Model;

class Book extends Model implements IBook
{
    protected $table = 'books';

    protected $fillable = [
        'name',
        'isbn',
        'description',
        'publicated',
        'format',
        'pages'
    ];

    protected $guarded = [
        'id'
    ];

    public function authors()
    {
        return $this->belongsToMany('App\Models\User', 'authors_books');
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBookAuthors(): array
    {
        $bookAuthors = [];
        //wtf??
        foreach ($this->authors()->get()->getDictionary() as $bookAuthor) {
            array_push($bookAuthors, $bookAuthor['attributes']);
        }
        return $bookAuthors;
    }

    public function getISBN(): string
    {
        return $this->isbn;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getYearOfPublication(): int
    {
        return $this->publicated;
    }

    public function getFormat(): int
    {
        return $this->format;
    }

    public function getPagesCount(): int
    {
        return $this->pages;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
