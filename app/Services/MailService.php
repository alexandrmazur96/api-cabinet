<?php
/**
 * Created by PhpStorm.
 * User: ep1demic
 * Date: 15.08.17
 * Time: 1:28
 */

namespace App\Services;

use App\Mail as Mailer;
use Illuminate\Support\Facades\Mail;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * In fact, it's mail sender :)
 *
 * Class MailService
 * @package App\Services
 */
class MailService
{
    /**
     * Gets info about email from queue RabbitMQ, analise and send email
     */
    public function run()
    {
        $connection = new AMQPStreamConnection(env('RABBIT_HOST'), env('RABBIT_PORT'), env('RABBIT_USER'), env('RABBIT_PASSWORD'));
        $channel = $connection->channel();

        $channel->queue_declare('mail', false, false, false, false);

        $callback = function ($msg) {
            $messageParams = json_decode($msg->body, true);
            $this->sendMail($messageParams['to'], $messageParams['type'], $messageParams['params']);
        };

        $channel->basic_consume('mail', '', false, true, false, false, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }

    /**
     * Sending email
     *
     * @param string $to Mail where will be send message
     * @param string $messageType Can be 'signUp' or 'remindPassword'
     * @param array $params Assoc array; For 'signUp' = key:['name','email','password']; For 'remindPassword' = key:['name','remindToken']
     * @return bool Indicate status of sending mail. True not guarantee that mail send will be success!
     */
    private function sendMail($to, $messageType, $params)
    {
        switch ($messageType) {
            case 'signUp':
                Mail::to($to)
                    ->send(new Mailer\SignUpMailer($params['name'], $params['email'], $params['password']));
                return true;
                break;
            case 'remindPassword':
                Mail::to($to)
                    ->send(new Mailer\RemindPasswordMailer($params['name'], $params['remindToken']));
                return true;
                break;
            default:
                return false;
                break;
        }

        return false;
    }
}