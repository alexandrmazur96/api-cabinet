<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemindPasswordMailer extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $remindToken;

    /**
     * Create a new message instance.
     *
     * @param string $name User name
     * @param string $remindToken User remindToken for change password. Needed for build remind-link
     */
    public function __construct($name, $remindToken)
    {
        $this->name = $name;
        $this->remindToken = $remindToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.remindPassword')
            ->with('name', $this->name)
            ->with('remindToken', $this->remindToken)
            ->from('info@thelibrary.ks', 'The Library')
            ->replyTo('info@thelibrary.ks', 'The Library')
            ->subject('The Library - восстановление пароля');
    }
}
