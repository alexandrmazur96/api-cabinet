<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SignUpMailer extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $email;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @param string $name User name
     * @param string $email User email
     * @param string $password User password
     */
    public function __construct($name, $email, $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.signUp')
            ->with('name', $this->name)
            ->with('email', $this->email)
            ->with('password', $this->password)
            ->from('info@thelibrary.ks', 'The Library')
            ->replyTo('info@thelibrary.ks', 'The Library')
            ->subject('The Library - регистрация');
    }
}
