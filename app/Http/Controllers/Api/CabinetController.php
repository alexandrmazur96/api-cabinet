<?php

namespace App\Http\Controllers\Api;

use App\Models\Email;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class CabinetController extends Controller
{
    /**
     * Get's info about current (logined) user.
     */
    public function currentUser(Request $request)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }

        $h_session = app('h_session');

        $userId = $h_session->getSessionData($request->header('x-session-id'))['id'];

        $user = User::find($userId);

        $currentUserData = [
            'id' => $user->id,
            'emails' => $user->getEmails(),
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ];

        return response(json_encode($currentUserData, JSON_UNESCAPED_UNICODE), 200);
    }

    /**
     * Get's info about side user by userId
     */
    public function sideUser(Request $request, $userId = null)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }

        $user = User::find($userId);

        if ($user !== null) {
            return response(json_encode($user, JSON_UNESCAPED_UNICODE), 200);
        } else {
            return response('User not found', 404);
        }
    }

    /**
     * Update current user profile (in this method - first_name & last_name)
     */
    public function updateCurrentUser(Request $request)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden');
        }

        $h_session = app('h_session');

        $userId = $h_session->getSessionData($request->header('x-session-id'))['id'];

        $changingData = $request->only(
            'first_name',
            'last_name'
        );

        if ($changingData['first_name'] === null && $changingData['last_name'] === null) {
            return response('Changing successful', 200);
        }

        if (strlen($changingData['first_name']) < 3) {
            return response('To short user first name', 302);
        }

        if (strlen($changingData['last_name']) < 3) {
            return response('To short user last name', 302);
        }

        $user = User::find($userId);

        if ($changingData['first_name'] !== null &&
            trim($changingData['first_name']) !== $user->first_name)
            $user->first_name = $changingData['first_name'];
        if ($changingData['last_name'] !== null &&
            trim($changingData['last_name']) !== $user->last_name)
            $user->last_name = $changingData['last_name'];

        $user->save();

        return response('Changing successful', 200);
    }

    /**
     * Add email to current (logined) user.
     */
    public function addUserEmail(Request $request)
    {
        $newEmail = $request->only('email');
        $validator = app('customValidator');
        switch ($validator->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }
        switch ($validator->checkEmail($newEmail)) {
            case 400:
                return response('Invalid param passed', 400);
            case 302:
                return response('Email exists', 302);
        }

        $h_session = app('h_session');

        $userId = $h_session->getSessionData($request->header('x-session-id'))['id'];

        (new Email([
            'email' => $newEmail['email'],
            'user_id' => $userId,
            'main' => 0
        ]))->save();

        return response('Email added', 200);
    }
}
