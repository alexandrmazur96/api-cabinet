<?php

namespace App\Http\Controllers\Api;

use App\Models\Book;
use App\Models\BookAuthor;
use App\Models\User;
use App\Models\UserBook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Gets current (logined) user books
     */
    public function getBooks(Request $request)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }

        $h_session = app('h_session');

        $userId = $h_session->getSessionData($request->header('x-session-id'))['id'];

        $user = User::find($userId);
        $books = $user->getBooks();
        if (!count($books)) {
            return response('Users books not found', 404);
        }

        return response(json_encode($books, JSON_UNESCAPED_UNICODE), 200);
    }

    /**
     * Find books by search term
     */
    public function searchBooks(Request $request, $term = null)
    {
        if ($term === null) {
            return response('Search term not passed', 400);
        }

        if (strlen($term) < 3) {
            return response('Search term to short', 400);
        }
        $books = Book::where('name', 'LIKE', '%' . $term . '%')
            ->orWhere('description', 'LIKE', '%' . $term . '%')
            ->get()->getDictionary();

        if (!count($books))
            return response('Books not found', 404);

        return response(json_encode($books, JSON_UNESCAPED_UNICODE), 200);
    }

    /**
     * Add book to logined user
     *
     * in header retrieve book-id and session-key
     */
    public function addBookToUser(Request $request)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }

        $h_session = app('h_session');

        $userId = $h_session->getSessionData($request->header('x-session-id'))['id'];
        $book = Book::find($request->header('x-book-id'));

        if ($book === null) {
            return response('Book not found', 404);
        }

        $isHave = UserBook::where('user_id', $userId)->where('book_id', $book->id)->first();
        if ($isHave !== null) {
            return response('Book exists in user', 200);
        }

        $userBook = new UserBook();
        $userBook->user_id = $userId;
        $userBook->book_id = $book->id;
        $userBook->save();

        return response('Book added', 200);
    }

    /**
     * Add book to store.
     * Not used now, need admin-panel and admin user
     */
    public function addBookToStore(Request $request)
    {
        $sessionCheck = app('customValidator');

        switch ($sessionCheck->checkSession($request->header('x-session-id'))) {
            case 401 :
                return response('Authorization required', 401);
            case 403 :
                return response('Access forbidden', 403);
        }

        $h_session = app('h_session');

        $userInfo = $h_session->getSessionData($request->header('x-session-id'));

        if (!User::find($userInfo['id'])->admin) {
            return response('Permission denied', 403);
        }

        $params = $request->only(
            'name',
            'isbn',
            'description',
            'publicated',
            'format',
            'pages',
            'authors'
        );
        $authors = json_decode($params['authors'], true, 512, JSON_UNESCAPED_UNICODE);
        $book = new Book($params);
        $book->save();

        foreach ($authors as $author) {
            $user = User::where('first_name', $author['first_name'])->where('last_name', $author['last_name'])->first();
            if ($user !== null && !$user->isAuthor()) {
                $user = new User($author);
                $user->save();
                $bookAuthor = new BookAuthor();
                $bookAuthor->user_id = $user->id;
                $bookAuthor->book_id = $book->id;
                $bookAuthor->save();

            } elseif ($user === null) {
                $user = new User($author);
                $user->save();
                $bookAuthor = new BookAuthor();
                $bookAuthor->user_id = $user->id;
                $bookAuthor->book_id = $book->id;
                $bookAuthor->save();
            } else {
                $bookAuthor = new BookAuthor();
                $bookAuthor->user_id = $user->id;
                $bookAuthor->book_id = $book->id;
                $bookAuthor->save();
            }
        }

        return response('Book added', 200);
    }

    /**
     * get book info by book-id
     */
    public function getBook(Request $request, $bookId)
    {
        $book = Book::find($bookId);
        if ($book === null) {
            return response('Book not found', 404);
        }
        $bookInfo['info'] = $book['attributes'];
        $bookAuthors['authors'] = $book->getBookAuthors();
        return response(json_encode($bookInfo + $bookAuthors, JSON_UNESCAPED_UNICODE), 200);
    }
}
