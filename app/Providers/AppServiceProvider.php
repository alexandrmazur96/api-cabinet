<?php

namespace App\Providers;

use App\Helpers\CustomValidator;
use App\Helpers\Session;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton('memcached', function ($app){
            $object = new \Memcached();
            $object->addServers(Config::get('cache.stores.memcached.servers'));

            return $object;
        });

        $this->app->singleton('customValidator', function($app){
            return new CustomValidator();
        });

        $this->app->singleton('h_session', function($app){
            return new Session();
        });
    }
}
